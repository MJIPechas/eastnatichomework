//
//  ViewController.swift
//  Homework
//
//  Created by Mohammad Jahir on 3/3/21.
//

import UIKit

class DayScheduleVC: UIViewController {
    
    //Declare All Variable
    
    var userIDcounter = 0
    //This one is for keeping track of Selected cell of Date Collection View (DateCV)
    var selectedItem = CalenderHelper.getCurrentDay() - 1
    
    lazy var container: UIView = {
        let uv = UIView()
        uv.backgroundColor = .productLightBlue
        return uv
    }()
    lazy var currentYearMonthLbl: UILabel = {
        let lbl = UILabel()
        lbl.text = ""
        lbl.textColor =  .white
        lbl.font = UIFont.systemFont(ofSize: RM.shared.height(20), weight: .bold)
        return lbl
    }()
    
    lazy var profileBtn: UIButton = {
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width:  RM.shared.height(38), height:  RM.shared.height(38)) )
        btn.backgroundColor = .white
        btn.layer.cornerRadius =  RM.shared.height(38/2)
        btn.clipsToBounds = true
        btn.setTitle("", for: .normal)
        btn.addTarget(self, action: #selector(invokedProfileBtn), for: .touchUpInside)
        btn.addShadow()
        return btn
        
    }()
    
    
    
    //MARK:- Date Area Collection View
    lazy var dateCV: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        //cv.allowsMultipleSelection = true
        cv.showsHorizontalScrollIndicator = false
        cv.register(DateCell.self, forCellWithReuseIdentifier: DateCellID.date)
        return cv
    }()
    
    //MARK:- Event Schedule Table View 
    lazy var scheduleTV: UITableView = {
        let tv = UITableView()
        
        tv.backgroundColor = .clear
        tv.separatorColor = .clear
        tv.register(ScheduleCell.self, forCellReuseIdentifier: ScheduleCellID.schedule)
        tv.dataSource = self
        tv.delegate = self
        return tv
    }()
    
    //Declare View Model For Fetching User List
    var authVM = AuthViewModel()
    var authList = [AuthModel](){
        didSet{
            initProfileBtn()
            printToConsole("Auth List Data Loaded")
        }
    }
    
    //Initialize this for populating Schedule Table View Data
    var activeEventListSInSelectedDate: [EventCustomModel] = []{
        didSet{
            scheduleTV.reloadData()
        }
    }
    //Create a dictionary for separating event according to day
    var eventDict: [String: [EventCustomModel]] = [:]
    
    var isCurrentDaySelected: Bool = false
    var eventVM = EventViewModel()
    var eventList = [EventModel](){
        didSet{
            self.profileBtn.isEnabled = true
            
            if eventList.count > 0{
                self.currentYearMonthLbl.text =  CalenderHelper.getYearMonth(date: eventList[0].eventDate ?? Common.dummyDate)
                
                dateCV.reloadData()
                let indexpath = IndexPath(item: CalenderHelper.getCurrentDay()+1, section: 0)
                self.dateCV.scrollToItem(at: indexpath, at: .centeredHorizontally, animated: true)
                isCurrentDaySelected = true
            }
            generateEventDictKeyWithList(day: CalenderHelper.getCurrentDay()-1)
            
        }
    }
    
    var eventDetailsVM = EventDetailsViewModel()
    var eventDetails = [EventDetailsModel](){
        didSet{
            printToConsole("Event Details Data Loaded")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        view.backgroundColor = .backgroundColor
        
        view.addSubview(container)
        view.addSubview(currentYearMonthLbl)
        view.addSubview(profileBtn)
        view.addSubview(dateCV)
        view.addSubview(scheduleTV)
        
        
        setupContainer()
        setupCurrentYrMnthLbl()
        setupProfileBtn()
        setupDateCV()
        setupScheduleTV()
        
        dateCV.delegate = self
        dateCV.dataSource = self
        
        
        fetchAuthList()
        
        //fetchEventDetails(userID: "0", eventID: "0")
        
        
    }
    
    //Change Status Bar Color to white
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //Profile Button Invoke Method
    @objc func invokedProfileBtn(){
        
        userIDcounter =  userIDcounter + 1 < authList.count ? userIDcounter + 1 : 0
        initProfileBtn()
    }
    
    //Implement profile button for Click to change profile
    func initProfileBtn(){
        let firstName = authList[userIDcounter].firstName?.prefix(1).uppercased() ?? ""
        let lastName = authList[userIDcounter].lastName?.prefix(1).uppercased() ?? ""
        let fullName = firstName + lastName
        profileBtn.setTitle(fullName, for: .normal)
        profileBtn.setTitleColor(.productDeepBlue, for: .normal)
        profileBtn.titleLabel?.font = UIFont.systemFont(ofSize: RM.shared.height(20), weight: .bold)
        fetchEventList(userID: "\(userIDcounter+1)")
        
    }
    
    //Genarate Dictionary key and get value according to the ket for population "Schedule Table View Data Source"
    func generateEventDictKeyWithList(day: Int){
        let key = CalenderHelper.getEventKeyBasedOnDay(day: day)
        activeEventListSInSelectedDate = eventDict[key] != nil ? eventDict[key]! : []
    }
    
    //Check that current day has any event or not for populating UI
    func dayHasEvent(day: Int) -> Bool{
        let key = CalenderHelper.getEventKeyBasedOnDay(day: day)
        return  eventDict[key] != nil ? true : false
    }
    
    //MARK:- Fetch Event List Data From API
    func fetchAuthList(){
        profileBtn.isEnabled = false
        authVM.fetchAuthList { (result) in
            switch result{
            case .success(let data):
                self.authList = data
            case .failure(let error):
                printToConsole("Auth Error \(error)")
            }
        }
    }
    //MARK:- Fetch Event List Data From API
    func fetchEventList(userID: String){
        //Disable Profile Button that user can not interact intime of fetching Event List according to UserID
        profileBtn.isEnabled = false
        
        eventVM.fetchEventList(completion: { (result) in
            switch result{
            case .success(let data):
                
                //Init event list
                self.eventList = data
                
                //Init Dictionary Key
                for event in self.eventList{
                    self.eventDict[CalenderHelper.getYearMonthDay(date: event.eventDate ?? "")] = []
                }
                
                
                let eventRange = self.eventList.count > 0 ? self.eventList.count-1 : 0
                
                if eventRange > 0{
                    
                    //Get A Random Index for showing a Current Event
                    let randomCurrentEventIndex = Int.random(in: 0...eventRange)
                    
                    for i in 0...eventRange{
                        
                        //Create a custom event model for better data population of event list in UI
                        var customEvent: EventCustomModel
                        
                        let event = self.eventList[i]
                        
                        
                        let randomEndTime = Int.random(in: 10...60)
                        
                        var timeDiff = 100
                        if i+1 < self.eventList.count{
                            // Get A time Difference from current event to Next Event for giving a proper random end time
                            timeDiff = CalenderHelper.twoEventTimeDiff(currentEventDate: event.eventDate ?? "", nextEventTime: self.eventList[i+1].eventDate ?? "")
                        }
                        
                        let startTime = CalenderHelper.getHourMin(date: event.eventDate ?? "")
                        let endTime = CalenderHelper.getExtendedHourMin(date: event.eventDate ?? "", extendedTime: timeDiff >= 60 ? randomEndTime : timeDiff)
                        
                        customEvent = EventCustomModel(event: event, startTime: startTime, endTime: endTime, isIdleState: false, isCurrentEvent: randomCurrentEventIndex == i ? true : false)
                        
                        //Init Dictionary Value With Custom Event Model
                        self.eventDict[CalenderHelper.getYearMonthDay(date: event.eventDate ?? "")]?.append(customEvent)
                        
                        if timeDiff >= 60 && i+1 < self.eventList.count{
                            
                            //Design a Gap a time
                            
                            let idleStarTime = CalenderHelper.getExtendedHourMin(date: event.eventDate ?? "", extendedTime: randomEndTime + 5)
                            let extendedtime = CalenderHelper.twoEventTimeDiff(currentEventDate: event.eventDate ?? "", nextEventTime: self.eventList[i+1].eventDate ?? "")
                            let idleEndTime = CalenderHelper.getExtendedHourMin(date: event.eventDate ?? "", extendedTime: extendedtime - (randomEndTime + 5) )
                            
                            customEvent = EventCustomModel(event: event, startTime: idleStarTime, endTime: idleEndTime, isIdleState: true, isCurrentEvent: false)
                            self.eventDict[CalenderHelper.getYearMonthDay(date: event.eventDate ?? "")]?.append(customEvent)
                        }
                        
                        
                    }
                }
   
            case .failure(let error):
                printToConsole("Event List Error \(error)")
            }
        }, userID: userID)
    }
    
    // Call Event Details API
    func fetchEventDetails(userID: String, eventID: String){
        eventDetailsVM.fetchEventDetails(completion: { (result) in
            switch result{
            case .success(let data):
                self.eventDetails = data
            
            case .failure(let error):
                printToConsole("Event Details Error \(error)")
            }
        }, userID: "2", eventID: "14")
    }
    
    
    
    //Below implemantation of Anchor of all Subviews
    func setupContainer(){
        
        container.anchor(top: view.topAnchor)
        container.setDimensions(height: view.frame.height/4.7, width: view.frame.width)
    }
    
    func setupCurrentYrMnthLbl(){
        currentYearMonthLbl.anchor(top: view.layoutMarginsGuide.topAnchor, left: view.layoutMarginsGuide.leftAnchor, right: view.layoutMarginsGuide.rightAnchor,  paddingTop: 3,  height: view.frame.height/25)
    }
    
    func setupProfileBtn(){
        profileBtn.anchor(top: view.layoutMarginsGuide.topAnchor, right: view.layoutMarginsGuide.rightAnchor, width: RM.shared.height(38) , height:  RM.shared.height(38))
    }
    
    func setupDateCV(){
        dateCV.anchor(top:currentYearMonthLbl.layoutMarginsGuide.bottomAnchor, left: view.layoutMarginsGuide.leftAnchor, bottom: container.bottomAnchor, right: view.rightAnchor, paddingTop: RM.shared.height(5))
        
    }
    
    func setupScheduleTV(){
        scheduleTV.anchor(top: container.layoutMarginsGuide.bottomAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.layoutMarginsGuide.rightAnchor, paddingTop: RM.shared.height(20))
    }
    
    
    
}

//MARK:- Extension
extension DayScheduleVC : UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    //MARK:- DateCV: Number of Item in section
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        /*Check Event List. Need at least "One Event" for initializing Collection View Cell.  Number of items: It Depends on "Event Month".*/
        
        return eventList.count > 0 ? CalenderHelper.daysInMonth(date: eventList.count > 0 ? eventList[0].eventDate ?? ""  : "") : 0
    }
    
    //MARK:- Date CV: Cell Init
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //Initialize UICollectionViewCell as Our CustomCollectionViewCell(DateCell)
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DateCellID.date, for: indexPath) as? DateCell  else {
            return UICollectionViewCell()
        }
        
        if eventList.count > 0 {
            //date:  Take date from event for getting Week Day of the month
            let date  = eventList[0].eventDate ?? Common.dummyDate
            
            //Passed data for Initializing DateCollectionView Cell Properties
            cell.setValue(dayNumber: indexPath.row, date: date, hasEvent: dayHasEvent(day: indexPath.row), isSelected: selectedItem == indexPath.row ? true : false)
        }

        return cell
    }
    
    
    //MARK:- DateCV: Select Cell
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        /* Generate Dictionary KEY and Populate Event Data in
        "Schedule Tabele View" for Selected date*/
        
        generateEventDictKeyWithList(day: indexPath.row)
 
        //Track the selected Item for Updating Data Source
        selectedItem = indexPath.row
        dateCV.reloadData()
        

    }
    
    //MARK:- DateCV:  Space Around Cell
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: RM.shared.height(20), left: 0, bottom: RM.shared.height(-20), right: 0)
    }
    
    //MARK:- DateCV:  Cell Size
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewWidth = collectionView.bounds.width
        let collectionViewHeight = collectionView.bounds.height
        return CGSize(width: collectionViewWidth/6.5 , height: collectionViewHeight/1.5)
    }
    
    
    
}
//MARK: - Schedule Table View
extension DayScheduleVC: UITableViewDelegate, UITableViewDataSource{
    
    //MARK:- Schedule TV: Number of Item
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return activeEventListSInSelectedDate.count
    }
    
    //MARK:- Schedule TV: Cell Init
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ScheduleCellID.schedule, for: indexPath) as? ScheduleCell  else {
            return UITableViewCell()
        }
        cell.setValue(event: activeEventListSInSelectedDate[indexPath.row])
        return cell
    }
    
    //MARK:- Schedule TV: Cell Size
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return activeEventListSInSelectedDate[indexPath.row].isIdleState ?? false ? RM.shared.height(50) : RM.shared.height(130)
    }
    
    
    
    
    
}
