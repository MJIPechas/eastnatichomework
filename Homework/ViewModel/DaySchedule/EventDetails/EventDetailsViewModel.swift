//
//  EventDetailsViewModel.swift
//  Homework
//
//  Created by Mohammad Jahir on 3/5/21.
//

import Foundation

class EventDetailsViewModel{
    
    func fetchEventDetails(completion: @escaping(Result<[EventDetailsModel],Error>) ->(), userID: String, eventID: String) {
        
        if let url = URL(string: ApiURL.eventDetails(userID: userID, eventID: eventID)){
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            let session = URLSession(configuration: .default)
            let task = session.dataTask(with: request){ (data, response, error) in
                if error == nil {
                    let decoder = JSONDecoder()
                    if let safeData = data{
                        do {
                            let result = try decoder.decode([EventDetailsModel].self, from: safeData)
                          
                            DispatchQueue.main.async {
                                completion(.success(result))
                            }
                            
                        }catch let jsonError{
                            completion(.failure(jsonError))
                            print("Some thing wrong there Related Movie")
                        }
                    }
                }
                
            }
            task.resume()
        }
    }
}
