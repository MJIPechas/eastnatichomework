//
//  AuthViewModel.swift
//  Homework
//
//  Created by Mohammad Jahir on 3/5/21.
//

import Foundation

class AuthViewModel{
    
    func fetchAuthList(completion: @escaping(Result<[AuthModel],Error>) ->()) {
        
        if let url = URL(string: ApiURL.auth()){
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            let session = URLSession(configuration: .default)
            let task = session.dataTask(with: request){ (data, response, error) in
                if error == nil {
                    let decoder = JSONDecoder()
                    if let safeData = data{
                        do {
                            let result = try decoder.decode([AuthModel].self, from: safeData)
                          
                            DispatchQueue.main.async {
                                completion(.success(result))
                            }
                            
                        }catch let jsonError{
                            completion(.failure(jsonError))
                            print("Some thing wrong there Related Movie")
                        }
                    }
                }
                
            }
            task.resume()
        }
    }
}
