//
//  DateCell.swift
//  Homework
//
//  Created by Mohammad Jahir on 3/4/21.
//

import UIKit

enum Subviews: Int{
    case dayNamuber = 0
    case weekDay = 1
    case indicator = 2
}

class DateCell: UICollectionViewCell {
    
    //Declare All Variable
    lazy var container:UIView = {
        let uv = UIView()
        uv.backgroundColor = .clear
        return uv
    }()
    
    lazy var dayNumber: UILabel = {
        let lbl = UILabel()
        lbl.text = ""
        lbl.textAlignment = .center
        lbl.textColor = .white
        lbl.font = UIFont.systemFont(ofSize: RM.shared.height(18), weight: .bold)
        lbl.addShadow()
        return lbl
    }()
    
    lazy var weekDay: UILabel = {
        let lbl = UILabel()
        lbl.text = ""
        lbl.textAlignment = .center
        lbl.textColor = .white
        lbl.font = UIFont.systemFont(ofSize: RM.shared.height(18), weight: .bold)
        lbl.addShadow()
        return lbl
    }()
    
    lazy var activeIndicator: UIView = {
        let uv = UIView()
        uv.backgroundColor = .backgroundColor
        uv.layer.cornerRadius = RM.shared.height(6)
        uv.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        return uv
    }()
    
    lazy var hasScheduleIndicator: UIView = {
        let uv = UIView()
        uv.backgroundColor = .backgroundColor
        uv.layer.cornerRadius = RM.shared.height(2)
        uv.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        return uv
    }()
    
    lazy var noScheduleIndicator: UIView = {
        let uv = UIView()
        uv.backgroundColor = .productDeepBlue
        uv.layer.cornerRadius = RM.shared.height(2)
        return uv
    }()
    
    
    
    lazy var dayWeekstackView: UIStackView = {
        let sv = UIStackView(arrangedSubviews: [ dayNumber, weekDay ])
        sv.axis = .vertical
        sv.distribution = .fill
        sv.setCustomSpacing(RM.shared.height(9), after: sv.subviews[Subviews.weekDay.rawValue])
        return sv
    }()
 
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        //Add All Sub View
        addSubview(container)
        addSubview(dayWeekstackView)
        addSubview(activeIndicator)
        addSubview(hasScheduleIndicator)
        addSubview(noScheduleIndicator)
        
        //Setup Anchor of All Subviews
        setupContainer()
        setupDayWeekStackView()
        setupActiveIndicator()
        setupHasScheduleIndicator()
        setupNoScheduleIndicator()
        
        //Intially Hide the view for proper Api Calling Fell
        hasScheduleIndicator.isHidden = true
        noScheduleIndicator.isHidden = true
        activeIndicator.isHidden = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //State When User Select A Day
    func selectedDay(){
        hasScheduleIndicator.isHidden = true
        noScheduleIndicator.isHidden = true
        activeIndicator.isHidden = false
        dayNumber.textColor = .white
        weekDay.textColor = .white
    }
    
    //State When A particular date has event
    func hasEvent(){
        hasScheduleIndicator.isHidden = false
        noScheduleIndicator.isHidden = true
        activeIndicator.isHidden = true
        dayNumber.textColor = .productDeepBlue
        weekDay.textColor = .productDeepBlue
        dayNumber.addShadow(shadowOpaciy: 0)
        weekDay.addShadow(shadowOpaciy: 0)
    }
    
    //State When A particular date has NO event
    func noEvent(){
        hasScheduleIndicator.isHidden = true
        noScheduleIndicator.isHidden = false
        activeIndicator.isHidden = true
        dayNumber.textColor = .productDeepBlue
        weekDay.textColor = .productDeepBlue
        dayNumber.addShadow(shadowOpaciy: 0)
        weekDay.addShadow(shadowOpaciy: 0)
    }
    
    //Populate property value
    func setValue(dayNumber: Int, date: String, hasEvent: Bool, isSelected: Bool){
        //add day by 1 because it start from Zero as I am taking from index path
        self.dayNumber.text = "\(dayNumber+1)"
        weekDay.text = CalenderHelper.weekDay(date: date,dayNumber: dayNumber)

        //Check and Implement view
        if isSelected{
            selectedDay()
        }else{
            if hasEvent{
                self.hasEvent()
            }else{
                self.noEvent()
            }
        }
        
       
    }
    
    //Below implemantation of Anchor of all Subviews
    // --- > RM: ResolutionManager for making UI more responside
    func setupContainer(){
        container.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor)
    }
    func setupDayWeekStackView(){
        dayWeekstackView.anchor(top: container.topAnchor, left: container.leftAnchor, right: container.rightAnchor, height: RM.shared.height(frame.height/1.4))
    }
    func setupActiveIndicator(){
        activeIndicator.anchor(top: dayWeekstackView.layoutMarginsGuide.bottomAnchor,  left: container.leftAnchor, bottom: container.bottomAnchor, right: container.rightAnchor, paddingTop: RM.shared.height(5))
       
    }
    
    func setupHasScheduleIndicator(){
        hasScheduleIndicator.anchor(top: dayWeekstackView.layoutMarginsGuide.bottomAnchor, bottom: container.bottomAnchor, paddingTop: RM.shared.height(5), width: RM.shared.width(frame.width/12))
        hasScheduleIndicator.centerX(inView: self)
        
    }
    
    func setupNoScheduleIndicator(){
        noScheduleIndicator.anchor(top: dayWeekstackView.layoutMarginsGuide.bottomAnchor, bottom: container.layoutMarginsGuide.bottomAnchor, paddingTop: RM.shared.height(2),  width: RM.shared.width(frame.width/11))
        noScheduleIndicator.centerX(inView: self)
    }
    
}


