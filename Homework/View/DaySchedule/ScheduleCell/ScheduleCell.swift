//
//  ScheduleCell.swift
//  Homework
//
//  Created by Mohammad Jahir on 3/4/21.
//

import UIKit

class ScheduleCell: UITableViewCell {
    
    var hasStackSubViews: [UIView] = []
    
    //Declare All Variable
    
    lazy var container: UIView = {
        let uv = UIView()
        uv.backgroundColor = .white
        uv.layer.cornerRadius = RM.shared.height(16)
        uv.layer.borderWidth = 1.3
        uv.layer.borderColor = UIColor.productLightBlue.cgColor
        uv.addShadow(shadowOpaciy: 0.1)
        return uv
    }()
    
    lazy var verticalLine: UIView = {
        let uv = UIView()
        uv.backgroundColor = .productLightGray
        return uv
    }()
    
    lazy var horizontalLine: UIView = {
        let uv = UIView()
        uv.backgroundColor = .productLightGray
        return uv
    }()
    
    lazy var idleStateContainer: UIView = {
        let uv = UIView()
        uv.backgroundColor = .white
        uv.layer.cornerRadius = RM.shared.height(8)
        uv.addShadow(shadowOpaciy: 0.1)
        return uv
    }()
    
    lazy var idleStateStartTime: UILabel = {
        let lbl = UILabel()
        lbl.text = "10:00"
        lbl.font = UIFont.systemFont(ofSize: RM.shared.height(14))
        lbl.textColor = .productLightGray
        return lbl
    }()
    lazy var idleStateEndTime: UILabel = {
        let lbl = UILabel()
        lbl.text = "10:45"
        lbl.font = UIFont.systemFont(ofSize: RM.shared.height(14))
        lbl.textColor = .productLightGray
        return lbl
    }()
    
    lazy var eventStartTime: UILabel = {
        let lbl = UILabel()
        lbl.text = "10:00"
        lbl.font = UIFont.systemFont(ofSize: RM.shared.height(14))
        lbl.textColor = .productBlack
        return lbl
    }()
    lazy var eventEndTime: UILabel = {
        let lbl = UILabel()
        lbl.text = "10:45"
        lbl.font = UIFont.systemFont(ofSize: RM.shared.height(14))
        lbl.textColor = .productGray
        return lbl
    }()
    lazy var eventType: UILabel = {
        let lbl = UILabel()
        lbl.text = "Legacy Communication Supervisor"
        lbl.font = UIFont.systemFont(ofSize: RM.shared.height(15), weight: .bold)
        lbl.adjustsFontSizeToFitWidth = true
        lbl.minimumScaleFactor = RM.shared.height(10)
        lbl.textColor = .productRed
        return lbl
    }()
    
    lazy var eventSubject: UILabel = {
        let lbl = UILabel()
        lbl.text = "Langosh - Reilly"
        lbl.font = UIFont.systemFont(ofSize: RM.shared.height(18), weight: .bold)
        lbl.adjustsFontSizeToFitWidth = true
        lbl.minimumScaleFactor = RM.shared.height(11)
        lbl.textColor = .productBlack
        return lbl
    }()
    
    lazy var eventAddress: UILabel = {
        let lbl = UILabel()
        lbl.text = "Langosh - Reilly"
        lbl.font = UIFont.systemFont(ofSize: RM.shared.height(13), weight: .regular)
        lbl.adjustsFontSizeToFitWidth = true
        lbl.minimumScaleFactor = RM.shared.height(9)
        lbl.textColor = .productGray
        return lbl
    }()
    
    lazy var hasLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "LB"
        lbl.textAlignment = .center
        lbl.backgroundColor = .productYellow
        lbl.clipsToBounds = true
        lbl.layer.cornerRadius = RM.shared.height(10)
        lbl.font = UIFont.systemFont(ofSize: RM.shared.height(15), weight: .regular)
        lbl.textColor = .white
        return lbl
    }()
    
    lazy var attachmentIcon: UIImageView = {
        let img = UIImageView()
        img.image = UIImage(systemName: "paperclip")
        img.contentMode = .scaleAspectFit
        img.tintColor = .productLightGray
        return img
    }()
    
    lazy var videoIcon: UIImageView = {
        let img = UIImageView()
        img.image = UIImage(systemName: "video.fill")
        img.contentMode = .scaleAspectFit
        img.tintColor = .productLightGray
        return img
    }()
    
    lazy var eventStack: UIStackView = {
        let sv = UIStackView(arrangedSubviews: [eventType, eventSubject, eventAddress])
        sv.axis = .vertical
        sv.distribution = .fillProportionally
        return sv
    }()
    

    lazy var hasStack: UIStackView = {
        let sv = UIStackView(arrangedSubviews:  hasStackSubViews )
        sv.axis = .horizontal
        sv.addVerticalSeparators(color: .productLightGray)
        sv.distribution = .fillProportionally
        sv.spacing = RM.shared.width(8)
        
        return sv
    }()
    
    lazy var ratingsLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "287.56"
        lbl.font = UIFont.systemFont(ofSize: RM.shared.height(15), weight: .regular)
        lbl.textColor = .productGray
        return lbl
    }()
    
    lazy var activeIndicator: UIImageView = {
        let img = UIImageView()
        img.image = UIImage(systemName: "play.fill")
        img.tintColor = .productDeepBlue
        img.contentMode = .scaleAspectFit
        return img
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle =  SelectionStyle.none
        backgroundColor = .clear
        
        //Add All Sub View
        
        addSubview(container)
        addSubview(eventStartTime)
        addSubview(eventEndTime)
        addSubview(verticalLine)
        addSubview(eventStack)
        addSubview(hasStack)
        addSubview(ratingsLabel)
        addSubview(activeIndicator)
        
        addSubview(idleStateContainer)
        addSubview(idleStateStartTime)
        addSubview(idleStateEndTime)
        addSubview(horizontalLine)
    
        
        idleStateContainer.isHidden = true
        
        
        //Setup Anchor of All Subviews
        
        activeEventSetup()
        
        setupIdleStateContainer()
        setupIdleStateStartTime()
        setupIdleStateEndTime()
        setupHorizontalLine()
    }

    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func activeEventSetup(){
        setupContainer()
        setupStartTime()
        setupEndTime()
        setupVerticalLine()
        setupEventStack()
        setupHasStack()
        setupRatingsLabel()
        setupActiveIndicator()
  

    }
    
    func inactiveEventSetup(){
        container.layer.borderColor = UIColor.clear.cgColor
        activeIndicator.isHidden = true
    }
    
    func hideEvent(){
        
        idleStateContainer.isHidden = false
        idleStateStartTime.isHidden = false
        idleStateEndTime.isHidden = false
        horizontalLine.isHidden = false
        
        container.isHidden = true
        eventStartTime.isHidden = true
        eventEndTime.isHidden = true
        verticalLine.isHidden = true
        eventType.isHidden = true
        eventSubject.isHidden = true
        eventAddress.isHidden = true
        ratingsLabel.isHidden = true
        activeIndicator.isHidden = true
    }
    
    func showEvent(){
        idleStateContainer.isHidden = true
        idleStateStartTime.isHidden = true
        idleStateEndTime.isHidden = true
        horizontalLine.isHidden = true
        
        container.isHidden = false
        eventStartTime.isHidden = false
        eventEndTime.isHidden = false
        verticalLine.isHidden = false
        eventType.isHidden = false
        eventSubject.isHidden = false
        eventAddress.isHidden = false
        ratingsLabel.isHidden = false
        activeIndicator.isHidden = false
 
    }
    
    func setValue( event: EventCustomModel? = nil ){
    
       //Check Event Status and Intialize value
        if event?.isIdleState ?? false{
            hideEvent()
            idleStateStartTime.text = event?.startTime
            idleStateEndTime.text = event?.endTime
        }else{
            if event?.isCurrentEvent ?? false{
                showEvent()
                container.layer.borderWidth = 1.3
                container.layer.borderColor = UIColor.productLightBlue.cgColor
                initValue(event: event)
            }else{
                showEvent()
                initValue(event: event)
                inactiveEventSetup()
            }
        }

    }
    
    //Intialize Event property value
    
    private func initValue(event: EventCustomModel? = nil){
        eventStartTime.text = event?.startTime
        eventEndTime.text = event?.endTime
        eventType.text = event?.event?.eventType
        eventSubject.text = event?.event?.eventSubject
        eventAddress.text = event?.event?.eventAddress
        ratingsLabel.text = event?.event?.rating
    }
    
    //Below implemantation of Anchor of all Subviews
    
    private func setupContainer(){
        container.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: RM.shared.height(5), paddingLeft: RM.shared.width(20), paddingBottom: RM.shared.height(5), paddingRight: RM.shared.width(2) )
    }
    
    private func setupStartTime(){
        eventStartTime.anchor(top: container.layoutMarginsGuide.topAnchor, left: container.layoutMarginsGuide.leftAnchor,  right: nil)
    }
    private func setupEndTime(){
        eventEndTime.anchor( left: container.layoutMarginsGuide.leftAnchor, bottom: container.layoutMarginsGuide.bottomAnchor, right: nil)
    }
    private func setupVerticalLine(){
        verticalLine.anchor(top: eventStartTime.layoutMarginsGuide.bottomAnchor, bottom: eventEndTime.layoutMarginsGuide.topAnchor, right:  eventStartTime.rightAnchor, paddingTop: RM.shared.height(5), paddingBottom: RM.shared.height(5), width: RM.shared.width(1.2))
    }
    private func setupEventStack(){
        eventStack.anchor(top: container.layoutMarginsGuide.topAnchor, left: eventStartTime.layoutMarginsGuide.rightAnchor, right: container.layoutMarginsGuide.rightAnchor, paddingLeft: RM.shared.width(20))
        eventStack.setDimensions(height:RM.shared.height(frame.height/0.7) , width:  RM.shared.width(frame.width/1.05))
        
       
    }
    private func setupHasStack(){
        hasStack.anchor(top: eventStack.layoutMarginsGuide.bottomAnchor, left: eventStartTime.layoutMarginsGuide.rightAnchor,  paddingTop: RM.shared.height(10), paddingLeft: RM.shared.width(20), paddingBottom: RM.shared.height(2))
        hasStack.setDimensions(height: RM.shared.height(frame.height/2.1), width:  RM.shared.width(CGFloat(30 * hasStack.subviews.count)) )
        
    }
    
    private func setupRatingsLabel(){
        ratingsLabel.anchor(top: eventStack.layoutMarginsGuide.bottomAnchor, right: container.layoutMarginsGuide.rightAnchor,  paddingTop: RM.shared.height(10), paddingLeft: RM.shared.width(20), paddingBottom: RM.shared.height(5))
    }
    
    private func setupActiveIndicator(){
        activeIndicator.anchor(left: leftAnchor, bottom: eventEndTime.layoutMarginsGuide.topAnchor,  width: RM.shared.height(16), height: RM.shared.height(16))
    }
    
    private func setupIdleStateContainer(){
        idleStateContainer.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: RM.shared.height(5), paddingLeft: RM.shared.width(20), paddingBottom: RM.shared.height(5), paddingRight: RM.shared.width(2) )
    }
    private func setupIdleStateStartTime(){
        idleStateStartTime.anchor( left: idleStateContainer.layoutMarginsGuide.leftAnchor,  right: nil)
        idleStateStartTime.centerY(inView: self)
    }
    
    private func setupIdleStateEndTime(){
        idleStateEndTime.anchor( right: idleStateContainer.layoutMarginsGuide.rightAnchor,  height: nil)
        idleStateEndTime.centerY(inView: self)
    }
    
    private func setupHorizontalLine(){
        horizontalLine.anchor(left: idleStateStartTime.rightAnchor, right: idleStateEndTime.leftAnchor, paddingLeft: RM.shared.width(8) , paddingRight: RM.shared.width(8), height: RM.shared.height(1))
        horizontalLine.centerY(inView: self)
    }
    
}
