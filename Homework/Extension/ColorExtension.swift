//
//  ColorExtension.swift
//  FollowFamily
//
//  Created by Mohammad Jahir on 1/27/21.
//

import UIKit

extension UIColor{
    static func rgb (r: CGFloat, g:CGFloat, b:CGFloat ) -> UIColor{
        return UIColor.init(red: r/255, green: g/255, blue: b/255, alpha: 1)
    }
    
    static let backgroundColor = UIColor.rgb(r: 242, g: 242, b: 242)
    static let productRed = UIColor.rgb(r: 227, g: 32, b: 61)
    static let productBlack = UIColor.rgb(r: 0, g: 0, b: 0)
    static let productGray = UIColor.rgb(r: 138, g: 138, b: 138)
    static let productLightGray = UIColor.rgb(r: 196, g: 196, b: 196)
    static let productYellow = UIColor.rgb(r: 285, g: 181, b: 0)
    static let productDeepBlue = UIColor.rgb(r: 63, g: 121, b: 144)
    static let productLightBlue = UIColor.rgb(r: 124, g: 179, b: 207)
}
