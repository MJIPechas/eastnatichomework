//
//  EventDetailsModel.swift
//  Homework
//
//  Created by Mohammad Jahir on 3/5/21.
//

import Foundation

struct EventDetailsModel: Codable {
    var id, eventID, eventComment: String?

    enum CodingKeys: String, CodingKey {
        case id
        case eventID = "eventId"
        case eventComment
    }
}
