//
//  AuthListModel.swift
//  Homework
//
//  Created by Mohammad Jahir on 3/5/21.
//

import Foundation

struct AuthModel: Codable {
    let userID, authToken: String?
    let avatar: String?
    let firstName, lastName, email, password: String?

    enum CodingKeys: String, CodingKey {
        case userID = "userId"
        case authToken, avatar, firstName, lastName, email, password
    }
}
