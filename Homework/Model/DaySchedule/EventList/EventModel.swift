//
//  EventListModel.swift
//  Homework
//
//  Created by Mohammad Jahir on 3/5/21.
//

import Foundation

struct EventModel: Codable {
    var id, userID, eventDate, eventType: String?
    var eventSubject, eventAddress: String?
    var hasAttachment, hasLabel, hasVideo: Bool?
    var rating: String?
    var important: Bool?

    enum CodingKeys: String, CodingKey {
        case id
        case userID = "userId"
        case eventDate, eventType, eventSubject, eventAddress, hasAttachment, hasLabel, hasVideo, rating, important
    }
}


struct EventCustomModel {
    var event: EventModel?
    var startTime: String?
    var endTime: String?
    var isIdleState: Bool?
    var isCurrentEvent: Bool?

}
