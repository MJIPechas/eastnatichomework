//
//  ApiURL.swift
//  Homework
//
//  Created by Mohammad Jahir on 3/5/21.
//

import Foundation

class ApiURL{
    
   static let baseURL = "https://6033d74f843b150017931b4a.mockapi.io/api/v1/"
    
    static func auth() -> String{
        return baseURL + "authenticate"
    }
    
    static func eventList(userID: String) -> String{
        return baseURL + "authenticate/\(userID)/events"
    }
    
    static func eventDetails(userID: String, eventID: String) -> String{
        return baseURL + "authenticate/\(userID)/events/\(eventID)/event"
    }
}
