//
//  ResoulationManager.swift
//  Robi MySports
//
//  Created by Mohammad Jahir on 12/10/19.
//  Copyright © 2019 ltech. All rights reserved.
//

import Foundation
import UIKit

public class RM{
   
    static let shared = RM()
    
    var bounds: CGRect = UIScreen.main.bounds
    let baseWidth: CGFloat =  414
    let baseHeight: CGFloat = 896
    
    
    
    func getDeviceWidth() -> CGFloat{
        return bounds.size.width
    }
    
    func getDeviceHeight() -> CGFloat{
        return bounds.size.height
    }
    func getNewsImageWidth() -> CGFloat{
        return bounds.size.width-30
    }

   
    func width(_ value: CGFloat) -> CGFloat{
            
        let resoulationDiffernece = baseWidth/getDeviceWidth()
        let actualWidth = CGFloat(value)/resoulationDiffernece

        return actualWidth
    }
    func height(_ value: CGFloat) -> CGFloat{

        let resoulationDiffernece = baseHeight/getDeviceHeight()
        let actualHeight = CGFloat(value)/resoulationDiffernece

        return actualHeight
    }
    
}
