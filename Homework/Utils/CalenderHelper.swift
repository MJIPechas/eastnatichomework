//
//  Calender.swift
//  Homework
//
//  Created by Mohammad Jahir on 3/3/21.
//

import Foundation
import UIKit

class CalenderHelper{
   
    static let calender = Calendar.current
    
    static func getFormattedDate(date: String) -> Date{
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat =  "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"

        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "yyyy MMMM" //"MMM dd,yyyy"

        let date: Date? = dateFormatterGet.date(from: date)
        
        //print("Date",dateFormatterPrint.string(from: date!)) // Date 2021 March
        //dateFormatterPrint.string(from: date!);
        return date ?? Date()
    }
    
    static func getCurrentMonth() -> String{
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "LLL"
        let nameOfMonth = dateFormatter.string(from: now)
        return nameOfMonth
    }
    
    static func getCurrentDay() -> Int{
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d"
        let day = dateFormatter.string(from: now)
        return Int(day) ?? 0
    }
    
    
    static func getCurrentYear() -> String{
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy"
        let nameOfMonth = dateFormatter.string(from: now)
        return nameOfMonth
    }
    
    static func getEventKeyBasedOnDay(day: Int) -> String{
        let day = "\(day+1)"
        let key = getCurrentMonth()+day+getCurrentYear()
        return key
    }
    
    
    static func getMonth(date: String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "LLLL"
        return dateFormatter.string(from: getFormattedDate(date: date))
    }
    
    static func getYearMonth(date: String) -> String{
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "yyyy MMMM"
        return dateFormatterPrint.string(from: getFormattedDate(date: date))
    }
    
    static func getYearMonthDay(date: String) -> String{
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "MMMdyyyy"
        return dateFormatterPrint.string(from: getFormattedDate(date: date));
    }
    
    static func getHourMin(date: String) -> String{
        let hour =  calender.component(.hour, from: getFormattedDate(date: date))
        let minutes = calender.component(.minute, from: getFormattedDate(date: date))
        return "\(hour):\(minutes)"
    }
    
    static func getExtendedHourMin(date: String, extendedTime: Int) -> String{
        
       
        let addTime = Calendar.current.date(byAdding: .minute, value: extendedTime, to: getFormattedDate(date: date))!
        let hour =  calender.component(.hour, from: addTime)
        let minutes = calender.component(.minute, from: addTime)
        return "\(hour):\(minutes)"
   
    }
    
    static func twoEventTimeDiff(currentEventDate: String, nextEventTime: String) -> Int{
        var hour =  calender.component(.hour, from: getFormattedDate(date: currentEventDate))
        var minutes = calender.component(.minute, from: getFormattedDate(date: currentEventDate))
        
        let totalMin = (hour * 60) + minutes
        
        let timeDiff = Calendar.current.date(byAdding: .minute, value: -totalMin, to: getFormattedDate(date: nextEventTime))!
        
        hour =  calender.component(.hour, from: timeDiff)
        minutes = calender.component(.minute, from: timeDiff)
        return (hour * 60) + minutes
    }
    
    func getYear(date: Date) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy"
        return dateFormatter.string(from: date)
    }
    static func daysInMonth(date: String)-> Int{
        let range = calender.range(of: .day, in: .month, for: getFormattedDate(date: date))!
        return range.count
    }
    
    static func weekDay(date: String, dayNumber: Int) -> String{
        let dateFormatter = DateFormatter()
        var weekday: String = ""
        dateFormatter.dateFormat = "ccc"
        let components = calender.dateComponents([.year, .month], from: getFormattedDate(date: date))
        let startOfMonth = calender.date(from: components) ?? Date()
        let actualDate = calender.date(byAdding: .day, value: dayNumber, to: startOfMonth)
        weekday = dateFormatter.string(from: actualDate!)
        return weekday
    }
    
}
