//
//  Common.swift
//  Homework
//
//  Created by Mohammad Jahir on 3/6/21.
//

import Foundation

class Common{
    static let dummyDate: String = "2021-03-04T10:09:37.947Z"
}
func printToConsole(_ message: String){
   #if DEBUG
   print(message)
   #endif
}
